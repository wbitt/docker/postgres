# The mobilitydb people keep changing tags suffix. 
# So check at their docker hub page.
FROM mobilitydb/mobilitydb:16-3.4-master

RUN     apt-get update \
    &&  apt-get -y install apt-utils apt-transport-https gnupg postgresql-common lsb-release wget \
    &&  sh -c "echo 'deb https://packagecloud.io/timescale/timescaledb/debian/ $(lsb_release -c -s) main' > /etc/apt/sources.list.d/timescaledb.list" \
    &&  wget --quiet -O - https://packagecloud.io/timescale/timescaledb/gpgkey | apt-key add - \
    &&  apt update \
    &&  /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh -y \
    &&  apt-get -y install \
          postgresql-16-ogr-fdw \
          postgresql-16-ogr-fdw-dbgsym \
          postgresql-16-partman \
          postgresql-16-pgrouting \
          postgresql-16-pgrouting-scripts \
          postgresql-16-pointcloud \
          postgresql-16-postgis-3 \
          postgresql-16-postgis-3-scripts \
          postgresql-16-postgis-3-dbgsym  \
          postgresql-16-pgvector \
          timescaledb-2-postgresql-16
          
